<?php

// Get the product data

$email = filter_input(INPUT_POST, 'email',FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$password = filter_input(INPUT_POST, 'password',FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$name = filter_input(INPUT_POST, 'name',FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$address = filter_input(INPUT_POST, 'address',FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$image = ($_FILES['image']['name']);
if (strlen($password) <= 8) {
    echo "<SCRIPT LANGUAGE='JavaScript'> alert('Oopsy , Password must be at least 8 characters!');  </SCRIPT>";
    include 'register_user_form.php';
}
$hashed_password = password_hash($password, PASSWORD_DEFAULT);

// Validate inputs
if ($email == null || $hashed_password == null || $name == null || $address == null) {
    echo "<SCRIPT LANGUAGE='JavaScript'> alert('Information not entered!');  </SCRIPT>";
    include 'register_user_form.php';
} else {
    $uppercase = preg_match('@[A-Z]@', $password);
    $lowercase = preg_match('@[a-z]@', $password);
    $number = preg_match('@[0-9]@', $password);
 $allowed = array('gif', 'png', 'jpg');
    if (!$uppercase || !$lowercase || !$number) {
        echo "<SCRIPT LANGUAGE='JavaScript'> alert('Password must be at least 8 characters!');  </SCRIPT>";
        exit();
    } else {
        require_once('database.php');

        // Add the product to the database 
        $query = "INSERT INTO customer
                 (customer_id,email, password, name,address, image)
              VALUES
                 (:customer_id,:email, :password, :name,:address,:image)";
        $statement = $db->prepare($query);
        $statement->bindValue(':customer_id', NULL);
        $statement->bindValue(':email', $email);
        $statement->bindValue(':password', $hashed_password);
        $statement->bindValue(':name', $name);
        $statement->bindValue(':address', $address);
        $statement->bindValue(':image', $image);
        $statement->execute();
        $statement->closeCursor();
        echo "<SCRIPT LANGUAGE='JavaScript'> alert('you have now registered procceed to log in');  </SCRIPT>";
        // Display the Product List page
        $target = "images/";
        $target = $target . basename($_FILES['image']['name']);
        if (move_uploaded_file($_FILES['image']['tmp_name'], $target)) {

            // Display the Product List page
            if (isset($_SESSION['privilages']) && $_SESSION['privilages'] == 1) {
                include ('view_admin.php');
            } else {
                include('view_customer.php');
            }
        } else {
            include 'index.php';
        }
    }
    }

?>
