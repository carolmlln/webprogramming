<?php
require_once('database.php');
require_once('session.php');
//
if (isset($_SESSION['privilages'])) {
    if ($_SESSION['privilages'] == 0) {
       include 'header/header2.php'; 
    
if ($_SESSION == 1) {
// Get products for selected category
    $queryCustomers = "SELECT * FROM customer";
    $statement3 = $db->prepare($queryCustomers);
    $statement3->execute();
    $customer = $statement3->fetchAll();
    $statement3->closeCursor();

    $queryTickets = "SELECT * FROM tickets";
    $statement4 = $db->prepare($queryTickets);
    $statement4->execute();
    $tickets = $statement4->fetchAll();
    $statement4->closeCursor();

    $queryReservation = "SELECT reservations.res_id,reservations.customer_id,tickets.ticket_id,tickets.image,reservations.quantity,tickets.price,reservations.quantity*tickets.price AS total FROM tickets INNER JOIN reservations ON reservations.ticket_id = tickets.ticket_id";
    $statement5 = $db->prepare($queryReservation);
    $statement5->execute();
    $reservations = $statement5->fetchAll();
    $statement5->closeCursor();

    $queryComment = "SELECT customer.customer_id, customer.name,comments.comments, comments.comment_id, comments.customer_id FROM customer LEFT JOIN comments ON comments.customer_id = customer.customer_id WHERE comments.comment_id > 0";
    $statement6 = $db->prepare($queryComment);
    $statement6->execute();
    $comments = $statement6->fetchAll();
    $statement6->closeCursor();

    $queryprice = "SELECT price FROM tickets Inner join customers on customer_id = customer_id";
    $statement7 = $db->prepare($queryprice);
    $statement7->execute();
    $prices = $statement6->fetchAll();
    $statement7->closeCursor();
} else {
    // Get products for selected category
    $queryCustomers = "SELECT * FROM customer WHERE customer_id = '" . $_SESSION['id'] . "'";
    $statement3 = $db->prepare($queryCustomers);
    $statement3->execute();
    $customer = $statement3->fetchAll();
    $statement3->closeCursor();

    $queryTickets = "SELECT * FROM tickets";
    $statement4 = $db->prepare($queryTickets);
    $statement4->execute();
    $tickets = $statement4->fetchAll();
    $statement4->closeCursor();

    $queryReservation = "SELECT reservations.res_id,reservations.customer_id,tickets.ticket_id,reservations.quantity,tickets.price,tickets.image,reservations.quantity*tickets.price AS total FROM tickets INNER JOIN reservations ON reservations.ticket_id = tickets.ticket_id WHERE customer_id = '" . $_SESSION['id'] . "'";
    $statement5 = $db->prepare($queryReservation);
    $statement5->execute();
    $reservations = $statement5->fetchAll();
    $statement5->closeCursor();

    $queryComment = "SELECT customer.customer_id, customer.name,comments.comments, comments.comment_id, comments.customer_id FROM customer LEFT JOIN comments ON comments.customer_id = customer.customer_id WHERE customer.customer_id = '" . $_SESSION['id'] . "'";
    $statement6 = $db->prepare($queryComment);
    $statement6->execute();
    $comments = $statement6->fetchAll();
    $statement6->closeCursor();

    $queryprice = "SELECT price FROM tickets Inner join customers on customer_id = customer_id WHERE customer_id = '" . $_SESSION['id'] . "'";
    $statement7 = $db->prepare($queryprice);
    $statement7->execute();
    $prices = $statement6->fetchAll();
    $statement7->closeCursor();
    
    
}
?>

 
            <div class="container">
                <h3> logged in as <?php echo $_SESSION['name'] ?></h3>
                <h1 class="mt-4 mb-3">Manage Your Information</h1>
                <div class="row">

                    <!-- Post Content Column -->
                    <div class="col-lg-8">
                        <section>
                            <h1>Your Details</h1>
                            <table class="table table-striped">
                                <thead class="thead-inverse">
                                    <tr>
                                        <th>Customer id</th>
                                        <th>Email</th>
                                        <th>Name</th>
                                        <th>Address</th>
                                        <th>Remove</th>
                                        <th>Edit</th>
                                    </tr>
                                </thead>
<?php foreach ($customer as $customers) : ?>
                                    <tr>
                                        <td><?php echo $customers['customer_id']; ?></td>
                                        <td><?php echo $customers['email']; ?></td>
                                        <td><?php echo $customers['name']; ?></td>
                                        <td><?php echo $customers['address']; ?></td>
                                        <td><form action="delete_account.php" method="post"
                                                  id="delete_account_form">
                                                <input type="hidden" name="customer_id"
                                                       value="<?php echo $customers['customer_id']; ?>">
                                                <input type="submit" value="Delete">
                                            </form></td>
                                        <td><form action="edit_user_form.php" method="post"
                                                  id="delete_user_form_form">
                                                <input type="hidden" name="customer_id"
                                                       value="<?php echo $customers['customer_id']; ?>">
                                                <input type="hidden" name="category_id"
                                                       value="<?php echo $customers['customer_id']; ?>">
                                                <input type="submit" value="Edit">
                                            </form></td>
                                    </tr>
<?php endforeach; ?>
                            </table >
                            <h1>Match List</h1>
                            <table class="table table-striped">
                                <thead class="thead-inverse">
                                    <tr>

                                        <th>Game</th>
                                        <th>Date</th>
                                        <th>Price</th>
                                    </tr>
                                </thead>
<?php foreach ($tickets as $ticket) : ?>
                                    <tr>
                                        <td><img src="images/<?php echo $ticket['image']; ?>" class="img-rounded" width="300px" height="150px" /></td>
                                        <td><?php echo $ticket['date']; ?></td>
                                        <td>€<?php echo number_format($ticket['price'], 2, '.', ''); ?></td>

                                    </tr>
<?php endforeach; ?>
                            </table>

                            <h1>Your Reservations</h1>
                            <table class="table table-striped">
                                <thead class="thead-inverse">
                                    <tr>

                                        <th>Match</th>
                                        <th>Amount</th>
                                        <th>Price Per Ticket</th>
                                        <th>Total Price</th>
                                        <th>Remove</th>
                                        <th>Edit</th>
                                    </tr>
                                </thead>
<?php foreach ($reservations as $reservation) : ?>
                                    <tr>
    <?php $reservation['res_id']; ?>
                                    <?php $reservation['customer_id']; ?>
                                        <td><img src="images/<?php echo $reservation['image']; ?>" class="img-rounded" width="300px" height="150px" /></td>
                                        <td><?php echo $reservation['quantity']; ?></td>
                                        <td>€<?php echo number_format($reservation['price'], 2, '.', ''); ?></td>
                                        <td>€<?php echo number_format($reservation['total'], 2, '.', ''); ?></td>
                                        <td><form action="delete_order.php" method="post"
                                                  id="delete_order_form">
                                                <input type="hidden" name="res_id"
                                                       value="<?php echo $reservation['res_id']; ?>">
                                                <input type="submit" value="Delete">
                                            </form></td>
                                        <td><form action="edit_order_form.php" method="post"
                                                  id="edit_order_form">
                                                <input type="hidden" name="res_id"
                                                       value="<?php echo $reservation['res_id']; ?>">
                                                <input type="hidden" name="res_id"
                                                       value="<?php echo $reservation['res_id']; ?>">
                                                <input type="submit" value="Edit">
                                            </form></td>


<?php endforeach; ?>



                            </table>

                            <button><a href="add_reservation_form.php">Add reservation</a></button>
                            <h1>Your Blog Comments</h1>
                            <table class="table table-striped">
                                <thead class="thead-inverse">
                                    <tr>
                                        <th>Name</th>
                                        <th>Comments</th>
                                        <th>Remove</th>
                                        <th>Edit</th>

                                    </tr>
                                </thead>
<?php foreach ($comments as $comment) : ?>
                                    <tr>
                                        <td><?php echo $comment['name']; ?></td>
                                        <td><?php echo $comment['comments']; ?></td>
                                        <td><form action="delete_comment.php" method="post"
                                                  id="delete_comment_form">
                                                <input type="hidden" name="comment_id"
                                                       value="<?php echo $comment['comment_id']; ?>">
                                                <input type="submit" value="Delete">
                                            </form></td> 
                                        <td><form action="edit_comment_form.php" method="post"
                                                  id="edit_comment_form">
                                                <input type="hidden" name="comment_id"
                                                       value="<?php echo $comment['comment_id']; ?>">
                                                <input type="hidden" name="comment_id"
                                                       value="<?php echo $comment['comment_id']; ?>">
                                                <input type="submit" value="Edit">
                                            </form></td>
                                    </tr>
<?php endforeach; ?>
                            </table>

                            <button><a href="add_comment_form.php">Add comment</a></button>
                        </section>


                    </div>
                    <!-- Sidebar Widgets Column -->
                    <div class="col-md-4">


                        <!-- Categories Widget -->
                        <div class="card my-4">
                            <h5 class="card-header">Useful Sites</h5>
                            <div class="card-block">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <ul class="list-unstyled mb-0">
                                            <li><a target="_blank"href="http://www.liverpoolfc.com/welcome-to-liverpool-fc">Liverpool FC </a></li>
                                            <li><a target="_blank"href="http://www.liverpoolfc.com/history/timeline">Liverpool FC History</a></li>
                                            <li><a target="_blank"href="https://store.liverpoolfc.com/">Liverpool Shop</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-6">

                                        <ul class="list-unstyled mb-0">
                                            <li><a target="_blank" href="https://www.livefootballtickets.com/english-premiership/liverpool-tickets.html?gclid=CjwKCAiA9MTQBRAREiwAzmytw_UmZUlzm2cn9gUnGEfcV0GO9Ck-EG7piVDXE95n-mv6Oq8_4V2cZhoCwwoQAvD_BwE">Live Football Tickets</a></li>
                                            <li><a target="_blank"href="https://www.ticketgum.com/liverpool-tickets?cmpn=Liverpool_world_exact&gclid=CjwKCAiA9MTQBRAREiwAzmytw98y8LdEEp5h4OxsHsthqslywMuTtReDHtFzWMHdku3dIBdK5El52RoCJ_cQAvD_BwE">Ticket Gum</a></li>
                                            <li><a target="_blank"href="http://www.liverpoolfc.com/tickets/tickets-availability">Liverpool FC Shop</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>



                </div><!-- End row -->
<button onclick="topFunction()" id="btn" title="Go to top" >Go to top</button>
            </div>           
<?php include('footer/footer.php');
} else if ($_SESSION['privilages'] == 1) {
        include_once 'view_admin.php';
    }
} else {
    
    include_once 'index.php';
}
?>
