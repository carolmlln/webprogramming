<?php
require_once('database.php');
require_once('session.php');

?>
<!DOCTYPE html>
<html>

<!-- the head section -->
<head>
        
   
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">   
        <link href="css/main.css" rel="stylesheet" type="text/css"/>

        <title>Liverpool Fc Home</title>
    <script type="text/javascript" src="js/clock.js"></script><!--clock JavaScript-->
    <script type="text/javascript" src="js/lytebox.js"></script><!--lytebox JavaScript-->
    <script type="text/javascript" src="js/checkEmail.js"></script><!--check email entry JavaScript-->
    <meta charset="UTF-8">
        
    </head>

<!-- the body section -->
<body>
    <header><h1>Liverpool</h1></header>

    <main>
        <h1>Database Error</h1>
        <p>There was an error connecting to the database.</p>
    </main>

<?php include('footer/footer.php');?>