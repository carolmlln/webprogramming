<?php
require_once('database.php');
include 'header/header1.php';
?>

            <div class="container">
                <!--Page Heading -->
                <h1 class="mt-4 mb-3">Register User</h1>
                <div class="row">

                    <!-- Post Content Column -->
                    <div class="col-lg-8">
                        <img class="img-fluid rounded" src="images/liverpoolfront.jpg" alt="" width="900" height="600">

                        <form action="register_user.php" method="post" id="add_user_form" enctype="multipart/form-data">

                            <label>Email:</label>
                            <input type="email" name="email"  pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,3}$" placeholder="Email"  title="please enter valid email">
                            <br>

                            <label>Password:</label>
                            <input name="password" type="password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"  placeholder="Password" title="please enter password starting with a capital with at least one capital at least one small case &#13; and at least one number with a length of at least 8" required>
                            <br>

                            <label>Name:</label>
                            <input type="input" name="name" placeholder="Name"  title="please your name">
                            <br>

                            <label>Address:</label>
                            <input type="input" name="address" placeholder="Address"  title="please enter your address">
                            <br> 
                            
                            <label>Avatar image:</label>
                            <input type="hidden" name="size" value="350000">
                            <input type="file" name="image">
                            <br>

                            <label>&nbsp;</label>
                            <input type="submit" value="Register">
                            <br>
                        </form>
                    </div>
                    <!-- Sidebar Widgets Column -->
                    <div class="col-md-4">


                        <!-- Categories Widget -->
                        <div class="card my-4">
                            <h5 class="card-header">Useful Sites</h5>
                            <div class="card-block">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <ul class="list-unstyled mb-0">
                                            <li><a target="_blank"href="http://www.liverpoolfc.com/welcome-to-liverpool-fc">Liverpool FC </a></li>
                                            <li><a target="_blank"href="http://www.liverpoolfc.com/history/timeline">Liverpool FC History</a></li>
                                            <li><a target="_blank"href="https://store.liverpoolfc.com/">Liverpool Shop</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-6">

                                        <ul class="list-unstyled mb-0">
                                            <li><a target="_blank" href="https://www.livefootballtickets.com/english-premiership/liverpool-tickets.html?gclid=CjwKCAiA9MTQBRAREiwAzmytw_UmZUlzm2cn9gUnGEfcV0GO9Ck-EG7piVDXE95n-mv6Oq8_4V2cZhoCwwoQAvD_BwE">Live Football Tickets</a></li>
                                            <li><a target="_blank"href="https://www.ticketgum.com/liverpool-tickets?cmpn=Liverpool_world_exact&gclid=CjwKCAiA9MTQBRAREiwAzmytw98y8LdEEp5h4OxsHsthqslywMuTtReDHtFzWMHdku3dIBdK5El52RoCJ_cQAvD_BwE">Ticket Gum</a></li>
                                            <li><a target="_blank"href="http://www.liverpoolfc.com/tickets/tickets-availability">Liverpool FC Shop</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>


                    

                        <div class="card my-4">
                            <h5 class="card-header">login</h5>
                            <div class="signin-form">

                                <div class="container small-container">


                                    <form class="log" action="login.php" method="POST" id="login">
                                        <div class="modal-body">
                                            <div id="div-login-msg">
                                                <div id="icon-login-msg" class="glyphicon glyphicon-chevron-right"></div>
                                                <span id="text-login-msg">Type your username and password.</span>
                                            </div>
                                            <input id="login_username" name="email" class="form-control" type="email" required pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,3}$" placeholder="email" title="please enter valid email" required>
                                            <input id="login_password" name="password" class="form-control" type="password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"  placeholder="Password" title="please enter password with at least one capital at least one small case &#13; and at least one number with a length of at least 8" required>
                                        </div>
                                        <div class="modal-footer">
                                            <div>
                                                <button name="" type="submit" class="btn btn-primary btn-group-justified btn-success">Login</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>



                        </div><!-- End 

                    </div>



                </div><!-- End row -->

            </div>           
<?php include('footer/footer.php');?>



