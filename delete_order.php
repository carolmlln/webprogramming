<?php
require_once('database.php');
require_once('session.php');

// Get IDs
$res_id = filter_input(INPUT_POST, 'res_id', FILTER_VALIDATE_INT,FILTER_SANITIZE_FULL_SPECIAL_CHARS);
echo $res_id;
// Delete the product from the database

    $query = "DELETE FROM reservations
              WHERE res_id = :res_id";
    $statement = $db->prepare($query);
    $statement->bindValue(':res_id', $res_id);
    $statement->execute();
    $statement->closeCursor();


// display the Product List page
if ($_SESSION['privilages'] == 1) {
    include ('view_admin.php');
} else {
    include('view_customer.php');
}
?>