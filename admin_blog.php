<?php
require_once('database.php');
require_once('session.php');
//include 'header/header3.php';

if (isset($_SESSION['privilages'])) {
    if ($_SESSION['privilages'] == 0) {
        include_once 'view_customer.php';
    } else if ($_SESSION['privilages'] == 1) {
include 'header/header3.php';
$queryComment = "SELECT customer.customer_id, customer.name,comments.comments, comments.comment_id, comments.customer_id FROM customer LEFT JOIN comments ON comments.customer_id = customer.customer_id WHERE comments.comment_id > 0";
$statement6 = $db->prepare($queryComment);
$statement6->execute();
$comments = $statement6->fetchAll();
$statement6->closeCursor();
?>

            <div class="container">
                <h3> Logged in as <?php echo $_SESSION['name'] ?></h3>

                <div class="row">

                    <!-- Post Content Column -->
                    <div class="col-lg-8">
                        <section>
                            <iframe style="margin-left: 10%;margin-top: 10%; text-align:center;" frameborder="5"  scrolling="no" width="500" height="400" src="https://www.fctables.com/england/premier-league/iframe/?type=league-scores&lang_id=2&country=67&template=10&team=189071&timezone=Europe/Dublin&time=24&width=520&height=440&font=Verdana&fs=12&lh=22&bg=FFFFFF&fc=333333&logo=1&tlink=1&scoreb=f4454f&scorefc=FFFFFF&sgdcoreb=8f8d8d&sgdcorefc=FFFFFF&sh=1&hfb=1&hbc=3bafda&hfc=FFFFFF"></iframe>         

                            <h1>Comment List</h1>

                            <?php foreach ($comments as $comment) : ?>
                                <table width="500" class="table table-hover table-inverse">
                                    <tr ><td><?php echo $comment['name']; ?></tr></td></br>                                       
                                    <tr height="200"><td><?php echo $comment['comments']; ?></tr></td></br>                                        





                                </table>
                                <tr>
                                <table>
                                    <form action="delete_comment.php" method="post"
                                          id="delete_comment_form">
                                        <input type="hidden" name="comment_id"
                                               value="<?php echo $comment['comment_id']; ?>">
                                        <input type="submit" value="Delete">
                                    </form> 
                                    <form action="edit_comment_form.php" method="post"
                                          id="edit_comment_form">
                                        <input type="hidden" name="comment_id"
                                               value="<?php echo $comment['comment_id']; ?>">
                                        <input type="hidden" name="comment_id"
                                               value="<?php echo $comment['comment_id']; ?>">
                                        <input type="submit" value="Edit">
                                    </form>
                                    </tr>
                                </table>
                                </br>
                            <?php endforeach; ?>
                            <button><a href="add_comment_form.php" width="400">Add comment</a></button>
                        </section>


                    </div>
                    <!-- Sidebar Widgets Column -->
                    <div class="col-md-4">


                        <!-- Categories Widget -->
                        <div class="card my-4">
                            <h5 class="card-header">Useful Sites</h5>
                            <div class="card-block">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <ul class="list-unstyled mb-0">
                                            <li><a target="_blank"href="http://www.liverpoolfc.com/welcome-to-liverpool-fc">Liverpool FC </a></li>
                                            <li><a target="_blank"href="http://www.liverpoolfc.com/history/timeline">Liverpool FC History</a></li>
                                            <li><a target="_blank"href="https://store.liverpoolfc.com/">Liverpool Shop</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-6">

                                        <ul class="list-unstyled mb-0">
                                            <li><a target="_blank" href="https://www.livefootballtickets.com/english-premiership/liverpool-tickets.html?gclid=CjwKCAiA9MTQBRAREiwAzmytw_UmZUlzm2cn9gUnGEfcV0GO9Ck-EG7piVDXE95n-mv6Oq8_4V2cZhoCwwoQAvD_BwE">Live Football Tickets</a></li>
                                            <li><a target="_blank"href="https://www.ticketgum.com/liverpool-tickets?cmpn=Liverpool_world_exact&gclid=CjwKCAiA9MTQBRAREiwAzmytw98y8LdEEp5h4OxsHsthqslywMuTtReDHtFzWMHdku3dIBdK5El52RoCJ_cQAvD_BwE">Ticket Gum</a></li>
                                            <li><a target="_blank"href="http://www.liverpoolfc.com/tickets/tickets-availability">Liverpool fC Shop</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>



                </div><!-- End row -->
<button onclick="topFunction()" id="btn" title="Go to top" >Go to top</button>
            </div>           
            <?php include('footer/footer.php'); 
            }
} else {

    include_once 'index.php';
}
