<?php

require_once('database.php');
require_once('session.php');
// Get the product data
$ticket_id = filter_input(INPUT_POST, 'ticket_id', FILTER_VALIDATE_INT, FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$game = filter_input(INPUT_POST, 'game', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$date = filter_input(INPUT_POST, 'date', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$price = filter_input(INPUT_POST, 'price', FILTER_VALIDATE_FLOAT, FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$image = ($_FILES['image']['name']);
// Validate inputs
if (empty($game) || empty($date) || empty($price)) {
    echo "<SCRIPT LANGUAGE='Invalid product data. Check all fields and try again.');  </SCRIPT>";
    include('add_match_form.php');
} else {
    $allowed = array('gif', 'png', 'jpg');
    // If valid, update the product in the database
    require_once('database.php');
    $query = 'UPDATE tickets
              SET 
                    
                  game = :game,
                  image = :image,
                  date = :date,
                  price = :price
               WHERE ticket_id = :ticket_id';
    $statement = $db->prepare($query);
    $statement->bindValue(':ticket_id', $ticket_id);
    $statement->bindValue(':game', $game);
    $statement->bindValue(':image', $image);
    $statement->bindValue(':date', $date);
    $statement->bindValue(':price', $price);
    $statement->execute();
    $statement->closeCursor();

    $target = "images/";
    $target = $target . basename($_FILES['image']['name']);
    if (move_uploaded_file($_FILES['image']['tmp_name'], $target)) {

        // Display the Product List page
        if ($_SESSION['privilages'] == 1) {
            include ('view_admin.php');
        } else {
            include('view_customer.php');
        }
    } else {
        include 'index.php';
    }
}
?>