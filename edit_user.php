<?php

require_once('database.php');
require_once('session.php');
// Get the product data
$customer_id = filter_input(INPUT_POST, 'customer_id', FILTER_VALIDATE_INT, FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$address = filter_input(INPUT_POST, 'address', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$privilages = filter_input(INPUT_POST, 'privilages', FILTER_SANITIZE_FULL_SPECIAL_CHARS);

// Validate inputs
if ($privilages == NULL || $email == NULL || $email == FALSE || empty($password) || empty($name) ||
        $address == NULL || $address == FALSE) {
    $error = "Invalid product data. Check all fields and try again.";
    include('error.php');
} else {
    
    // If valid, update the product in the database
    $query = 'UPDATE customer
              SET 
                  privilages = :privilages ,
                  email = :email,
                  password = :password,
                  name = :name,
                  address = :address
                  
               WHERE customer_id = :customer_id';
    $statement = $db->prepare($query);
    $statement->bindValue(':customer_id', $customer_id);
    $statement->bindValue(':privilages', $privilages);
    $statement->bindValue(':email', $email);
    $statement->bindValue(':password', $password);
    $statement->bindValue(':name', $name);
    $statement->bindValue(':address', $address);
    $statement->execute();
    $statement->closeCursor();

    if ($_SESSION['privilages'] == 1) {
        include ('view_admin.php');
    } else {
        include('view_customer.php');
    }
}
?>