<?php
require_once('database.php');
require_once('session.php');

// Get IDs
$ticket_id = filter_input(INPUT_POST, 'ticket_id', FILTER_VALIDATE_INT,FILTER_SANITIZE_FULL_SPECIAL_CHARS);

// Delete the product from the database

    $query = "DELETE FROM tickets
              WHERE ticket_id = :ticket_id";
    $statement = $db->prepare($query);
    $statement->bindValue(':ticket_id', $ticket_id);
    $statement->execute();
    $statement->closeCursor();


// display the Product List page
if ($_SESSION['privilages'] == 1) {
    include ('view_admin.php');
} else {
    include('view_customer.php');
}
?>