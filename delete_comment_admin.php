<?php
require_once('database.php');
require_once('session.php');

// Get IDs
$comment_id = filter_input(INPUT_POST, 'comment_id', FILTER_VALIDATE_INT,FILTER_SANITIZE_FULL_SPECIAL_CHARS);

// Delete the product from the database

    $query = "DELETE FROM comments
              WHERE comment_id = :comment_id";
    $statement = $db->prepare($query);
    $statement->bindValue(':comment_id', $comment_id);
    $statement->execute();
    $statement->closeCursor();


// display the Product List page
if ($_SESSION['privilages'] == 1) {
    include ('view_admin.php');
} else {
    include('view_customer.php');
}
?>