<?php

include_once 'session.php';
include_once 'database.php';

$email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL,FILTER_SANITIZE_SPECIAL_CHARS);
$password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_FULL_SPECIAL_CHARS);

if ($email == NULL || $password == NULL)
{
    $errMsg = "Please enter valid data";
    include("database_error.php");
    exit();
}
//checkig password length
if (strlen($password) <= 8)
{
    echo "<SCRIPT LANGUAGE='JavaScript'> alert('Password must be at least 8 characters!');</SCRIPT>";
    exit();
}
else
{
    $uppercase = preg_match('@[A-Z]@', $password);
    $lowercase = preg_match('@[a-z]@', $password);
    $number = preg_match('@[0-9]@', $password);

    if (!$uppercase || !$lowercase || !$number)
    {
        echo 'Length of password insufficent';
        exit();
    }
    
    else
    {
        $errMsg = '';

        $query = 'SELECT customer_id, privilages, email, password,image 
                FROM customer
                WHERE email = :email';
        $records = $db->prepare($query);
        $records->bindParam(':email', $email);
        $records->execute();
        $results = $records->fetch(PDO::FETCH_ASSOC);
        $records->closeCursor();

        if (count($results) > 0 && password_verify($password, $results['password']) && $results['privilages'] == 0)
        {
            $errMsg .= 'admin ';
            $_SESSION['id'] = filter_var($results['customer_id'], FILTER_VALIDATE_INT,FILTER_SANITIZE_FULL_SPECIAL_CHARS);
            $_SESSION['privilages'] = filter_var($results['privilages'], FILTER_VALIDATE_INT,FILTER_SANITIZE_FULL_SPECIAL_CHARS);
            $_SESSION['name'] = filter_var($results['email'], FILTER_SANITIZE_FULL_SPECIAL_CHARS);
            $_SESSION['image'] = filter_var($results['image'], FILTER_SANITIZE_FULL_SPECIAL_CHARS);
            $_SESSION['time_out']= time();

//            header('location: index.php');
            include 'user_index.php';
            exit;
        }
        if (count($results) > 0 && password_verify($password, $results['password']) && $results['privilages'] == 1)
        {
            $errMsg .= ' user ';
            $_SESSION['id'] = filter_var($results['customer_id'], FILTER_VALIDATE_INT,FILTER_SANITIZE_FULL_SPECIAL_CHARS);
            $_SESSION['privilages'] = filter_var($results['privilages'], FILTER_VALIDATE_INT,FILTER_SANITIZE_FULL_SPECIAL_CHARS);
            $_SESSION['name'] = filter_var($results['email'], FILTER_SANITIZE_FULL_SPECIAL_CHARS);
            $_SESSION['image'] = filter_var($results['image'], FILTER_SANITIZE_FULL_SPECIAL_CHARS);
            $_SESSION['time_out']= time();

//            header('location: index.php');
            include 'full_admin_index.php';
            
            exit;
        }
        else
        {
            $errMsg .= 'Login Failed, Incorrect details entered!';
        }
    }
    if (isset($errMsg))
    {
        echo "<script type='text/javascript'> alert(" . json_encode($errMsg) . "); window.location=document.referrer;</script>";
    }
}


?>



