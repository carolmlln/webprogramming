<?php
require_once('database.php');
require_once('session.php');
// Get IDs
$customer_id = filter_input(INPUT_POST, 'customer_id', FILTER_VALIDATE_INT,FILTER_SANITIZE_FULL_SPECIAL_CHARS);

// Delete the product from the database

    $query = "DELETE FROM customer
              WHERE customer_id = :customer_id";
    $statement = $db->prepare($query);
    $statement->bindValue(':customer_id', $customer_id);
    $statement->execute();
    $statement->closeCursor();


// display the Product List page
if ($_SESSION['privilages'] == 1) {
    include ('view_admin.php');
} else {
    include('index.php');
}
?>