<?php
require_once('database.php');
require_once('session.php');
// Get the product data
$customer_id = filter_input(INPUT_POST, 'customer_id',FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$comment = filter_input(INPUT_POST, 'comments',FILTER_SANITIZE_FULL_SPECIAL_CHARS);



// Validate inputs
if (empty($customer_id)|| empty($comment)) {
    $error = "Invalid product data. Check all fields and try again.";
    include('error.php');
} else {
    require_once('database.php');

    // Add the product to the database 
    $query = "INSERT INTO comments
                 (comment_id,customer_id,comments)
              VALUES
                 (:comment_id,:customer_id,:comments)";
    $statement = $db->prepare($query);
    $statement->bindValue(':comment_id', NULL);
    $statement->bindValue(':customer_id', $customer_id);
    $statement->bindValue(':comments', $comment);
    $statement->execute();
    $statement->closeCursor();

    // Display the Product List page
   if ($_SESSION['privilages'] == 1) {
        include ('view_admin.php');
    } else {
        include('view_customer.php');
    }
}
?>