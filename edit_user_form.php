<?php
require_once('database.php');
require_once('session.php');

$customer_id = filter_input(INPUT_POST, 'customer_id', FILTER_VALIDATE_INT, FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$query = 'SELECT *
          FROM customer
          WHERE customer_id = :customer_id';
$statement = $db->prepare($query);
$statement->bindValue(':customer_id', $customer_id);
$statement->execute();
$customer = $statement->fetch();
$statement->closeCursor();
?>
<!DOCTYPE html>
<html>
    <head>


        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">   
        <link href="css/main.css" rel="stylesheet" type="text/css"/>

        <title>Liverpool FC Home</title>
        <script type="text/javascript" src="js/clock.js"></script><!--clock JavaScript-->
        <script type="text/javascript" src="js/lytebox.js"></script><!--lytebox JavaScript-->
        <script type="text/javascript" src="js/checkEmail.js"></script><!--check email entry JavaScript-->
        <meta charset="UTF-8">

    </head>

    <body id="background">
        <section class="container main-wrapper">
            <header>
                <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                    <a class="navbar-brand" href="#">Liverpool</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                            <!--Add left menu items here-->
                        </ul>

                        <form class="form-inline my-2 my-lg-0">
                            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">

                                <li class="nav-item">
                                    <a class="nav-link" href="full_admin_index.php">Home</a>

                                </li> 
                                <li class="nav-item">
                                    <a class="nav-link" href="admin_history.php">History</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="admin_blog.php">Blog</a>
                                </li> 

                                <li class="nav-item">
                                    <a class="nav-link" href="view_admin.php">Database</a>
                                </li> 
                                <li class="nav-item">
                                    <a class="nav-link" href="logout.php">LogOut</a>
                                </li>  

                            </ul>
                        </form>
                    </div>
                </nav>
            </header>       
            <div class="container">
                <!--Page Heading -->
                <h1 class="mt-4 mb-3">Manage User</h1>
                <div class="row">

                    <!-- Post Content Column -->
                    <div class="col-lg-8">
                        <img class="img-fluid rounded" src="images/liverpoolfront.jpg" alt="" width="900" height="600">
                        <h1>Edit Product</h1>
                        <form action="edit_user.php" method="post"
                              id="add_user_form_form">
                                  <?php
                                  if ($_SESSION['privilages'] == 1) {
                                      ?>
                                <label>Privilages:</label>
                                <input type="number" name="privilages"
                                       value="<?php echo $customer['privilages']; ?>">
                                       <?php
                                       } else {
                                       ?>
                                <input type="hidden" name="privilages"
                                       value="<?php echo $customer['privilages']; ?>">
                                <br>
                                <?php }
                                ?>       
                                <input type="hidden" name="customer_id"
                                       value="<?php echo $customer['customer_id']; ?>">
                                </br>
                                
                                <label>Email:</label>
                                <input type="email" name="email"
                                       value="<?php echo $customer['email']; ?>">
                                <input type="hidden" name="password"
                                       value="<?php echo $customer['password']; ?>">
                                <br>

                                <label>Name:</label>
                                <input type="input" name="name"
                                       value="<?php echo $customer['name']; ?>">
                                <br>

                                <label>Address:</label>
                                <input type="input" name="address"
                                       value="<?php echo $customer['address']; ?>">
                                <br>
                                <label>&nbsp;</label>
                                <input type="submit" value="Save Changes">
                                <br>
                            </form>

                        </div>
                        <!-- Sidebar Widgets Column -->
                        <div class="col-md-4">


                        <!-- Categories Widget -->
                        <div class="card my-4">
                            <h5 class="card-header">Useful Sites</h5>
                            <div class="card-block">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <ul class="list-unstyled mb-0">
                                            <li><a target="_blank"href="http://www.liverpoolfc.com/welcome-to-liverpool-fc">Liverpool FC </a></li>
                                            <li><a target="_blank"href="http://www.liverpoolfc.com/history/timeline">Liverpool FC History</a></li>
                                            <li><a target="_blank"href="https://store.liverpoolfc.com/">Liverpool Shop</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-6">

                                        <ul class="list-unstyled mb-0">
                                            <li><a target="_blank" href="https://www.livefootballtickets.com/english-premiership/liverpool-tickets.html?gclid=CjwKCAiA9MTQBRAREiwAzmytw_UmZUlzm2cn9gUnGEfcV0GO9Ck-EG7piVDXE95n-mv6Oq8_4V2cZhoCwwoQAvD_BwE">Live Football Tickets</a></li>
                                            <li><a target="_blank"href="https://www.ticketgum.com/liverpool-tickets?cmpn=Liverpool_world_exact&gclid=CjwKCAiA9MTQBRAREiwAzmytw98y8LdEEp5h4OxsHsthqslywMuTtReDHtFzWMHdku3dIBdK5El52RoCJ_cQAvD_BwE">Ticket Gum</a></li>
                                            <li><a target="_blank"href="http://www.liverpoolfc.com/tickets/tickets-availability">Liverpool FC Shop</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>



                    </div><!-- End row -->

                </div>           
                <?php include('footer/footer.php'); ?>
