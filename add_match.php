<?php

require_once('database.php');
require_once('session.php');
// Get the product data

$game = filter_input(INPUT_POST, 'game', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$date = filter_input(INPUT_POST, 'date', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$price = filter_input(INPUT_POST, 'price', FILTER_VALIDATE_FLOAT, FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$image = ($_FILES['image']['name']);


// Validate inputs
if (empty($game) ||  empty($date) || empty($price)) {
    echo "<SCRIPT LANGUAGE='Invalid product data. Check all fields and try again.');  </SCRIPT>";
    include('add_match_form.php');
} else {
    $allowed = array('gif', 'png', 'jpg');

    require_once('database.php');

    // Add the product to the database 
    $query = "INSERT INTO tickets
                 (ticket_id,game, image, date,price)
              VALUES
                 (:ticket_id,:game, :image, :date,:price)";
    $statement = $db->prepare($query);
    $statement->bindValue(':ticket_id', NULL);
    $statement->bindValue(':game', $game);
    $statement->bindValue(':image', $image);
    $statement->bindValue(':date', $date);
    $statement->bindValue(':price', $price);
    $statement->execute();
    $statement->closeCursor();

    $target = "images/";
    $target = $target . basename($_FILES['image']['name']);
    if (move_uploaded_file($_FILES['image']['tmp_name'], $target)) {

        // Display the Product List page
        if ($_SESSION['privilages'] == 1) {
            include ('view_admin.php');
        } else {
            include('view_customer.php');
        }
    } else {
        include 'index.php';
    }
}
?>