<?php
require_once('database.php');
include 'header/header1.php';
// Get products for selected category
$queryCustomers = "SELECT * FROM customer";
$statement3 = $db->prepare($queryCustomers);
$statement3->execute();
$customer = $statement3->fetchAll();
$statement3->closeCursor();

$queryTickets = "SELECT * FROM tickets";
$statement4 = $db->prepare($queryTickets);
$statement4->execute();
$tickets = $statement4->fetchAll();
$statement4->closeCursor();

$queryReservation = "SELECT * FROM reservations";
$statement5 = $db->prepare($queryReservation);
$statement5->execute();
$reservations = $statement5->fetchAll();
$statement5->closeCursor();

$queryComment = "SELECT customer.customer_id, customer.name,customer.image,comments.comments, comments.comment_id,comments.date, comments.customer_id FROM customer LEFT JOIN comments ON comments.customer_id = customer.customer_id WHERE comments.comment_id > 0";
$statement6 = $db->prepare($queryComment);
$statement6->execute();
$comments = $statement6->fetchAll();
$statement6->closeCursor();
?>



<div class="container">
    <h1 class="mt-4 mb-3">Blog</h1>
    <div class="row">

        <!-- Post Content Column -->
        <div class="col-lg-8">
            <section>
                <iframe style="margin-left: 10%;margin-top: 10%; text-align:center;" frameborder="5"  scrolling="no" width="520" height="320" src="https://www.fctables.com/england/premier-league/iframe/?type=league-scores&lang_id=2&country=67&template=10&team=189071&timezone=Europe/Dublin&time=24&width=520&height=440&font=Verdana&fs=12&lh=22&bg=FFFFFF&fc=333333&logo=1&tlink=1&scoreb=f4454f&scorefc=FFFFFF&sgdcoreb=8f8d8d&sgdcorefc=FFFFFF&sh=1&hfb=1&hbc=3bafda&hfc=FFFFFF"></iframe>         

                <h1>Comment List</h1>

                <?php foreach ($comments as $comment) : ?>
                    <table width="500" class="table table-hover table-inverse">    
                        <th><img src="images/<?php echo $comment['image']; ?>" class="img-rounded" width="150px" height="150px" /></th>
                        <th colspan=""><p style="text-align: center;"><?php echo $comment['name']; ?></p></th></tr></br>                                       
                        <tr height="200"><td><?php echo $comment['comments']; ?></td></tr></br>                                        
                        <tr height="200"><td><?php echo $comment['date']; ?></td></tr></br>
                    </table> 
                    </br>

                <?php endforeach; ?>


            </section>


        </div>
        <!-- Sidebar Widgets Column -->
        <div class="col-md-4">


            <!-- Categories Widget -->
            <div class="card my-4">
                <h5 class="card-header">Useful Sites</h5>
                <div class="card-block">
                    <div class="row">
                        <div class="col-lg-6">
                            <ul class="list-unstyled mb-0">
                                <li><a target="_blank"href="http://www.liverpoolfc.com/welcome-to-liverpool-fc">Liverpool FC </a></li>
                                <li><a target="_blank"href="http://www.liverpoolfc.com/history/timeline">Liverpool FC History</a></li>
                                <li><a target="_blank"href="https://store.liverpoolfc.com/">Liverpool Shop</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-6">

                            <ul class="list-unstyled mb-0">
                                <li><a target="_blank" href="https://www.livefootballtickets.com/english-premiership/liverpool-tickets.html?gclid=CjwKCAiA9MTQBRAREiwAzmytw_UmZUlzm2cn9gUnGEfcV0GO9Ck-EG7piVDXE95n-mv6Oq8_4V2cZhoCwwoQAvD_BwE">Live Football Tickets</a></li>
                                <li><a target="_blank"href="https://www.ticketgum.com/liverpool-tickets?cmpn=Liverpool_world_exact&gclid=CjwKCAiA9MTQBRAREiwAzmytw98y8LdEEp5h4OxsHsthqslywMuTtReDHtFzWMHdku3dIBdK5El52RoCJ_cQAvD_BwE">Ticket Gum</a></li>
                                <li><a target="_blank"href="http://www.liverpoolfc.com/tickets/tickets-availability">Liverpool fC Shop</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>





            <!-- Side Widget -->
            <div class="card my-4">
                <h5 class="card-header">login</h5>
                <div class="card-block">
                    <form class="log" action="login.php" method="POST" id="login">
                        <div class="modal-body">
                            <div id="div-login-msg">
                                <div id="icon-login-msg" class="glyphicon glyphicon-chevron-right"></div>
                                <span id="text-login-msg">Type your username and password.</span>
                            </div>
                            <input id="login_username" name="email" class="form-control" type="email" required pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,3}$" placeholder="email" title="please enter valid email" required>
                            <input id="login_password" name="password" class="form-control" type="password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"  placeholder="Password" title="please enter password with at least one capital at least one small case &#13; and at least one number with a length of at least 8" required>
                        </div>
                        <div class="modal-footer">
                            <div>
                                <button name="" type="submit" class="btn btn-primary btn-group-justified btn-success">Login</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>



    </div><!-- End row -->
    <button onclick="topFunction()" id="btn" title="Go to top" >Go to top</button>
</div>           
<?php include('footer/footer.php'); ?>






























