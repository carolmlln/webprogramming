<html>
    <head>


        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">   
        <link href="css/main.css" rel="stylesheet" type="text/css"/>

        <title>Liverpool Fc Home</title>
        <script type="text/javascript" src="js/clock.js"></script><!--clock JavaScript-->
        <script type="text/javascript" src="js/lytebox.js"></script><!--lytebox JavaScript-->
        <script type="text/javascript" src="js/checkEmail.js"></script><!--check email entry JavaScript-->
        <script src="jquery/scroll.js" type="text/javascript"></script>
        <meta charset="UTF-8">

    </head>


    <body id="background">
        <section class="container main-wrapper">
            <header>
                <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                    <a class="navbar-brand" href="#">Liverpool</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                            <!--Add left menu items here-->
                        </ul>

                        <form class="form-inline my-2 my-lg-0">
                            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">

                                <li class="nav-item">
                                    <a class="nav-link" href="user_index.php">Home</a>

                                </li> 
                                <li class="nav-item">
                                    <a class="nav-link" href="user_history.php">History</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="blog.php">Live</a>
                                </li> 
                                <li class="nav-item">
                                    <a class="nav-link" href="sports_blog.php">Blog</a>
                                </li> 

                                <li class="nav-item">
                                    <a class="nav-link" href="view_customer.php">Database</a>
                                </li> 
                                <li class="nav-item">
                                    <a class="nav-link" href="logout.php">LogOut</a>
                                </li>  

                            </ul>
                        </form>
                    </div>
                </nav>
