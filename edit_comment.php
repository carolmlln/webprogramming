<?php

require_once('database.php');
require_once('session.php');

$comment_id = filter_input(INPUT_POST, 'comment_id', FILTER_VALIDATE_INT,FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$customer_id = filter_input(INPUT_POST, 'customer_id', FILTER_VALIDATE_INT,FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$comments = filter_input(INPUT_POST, 'comments',FILTER_SANITIZE_FULL_SPECIAL_CHARS);


// Validate inputs
if (empty($comment_id) || empty($customer_id) || empty($comments)) {
    $error = "Invalid product data. Check all fields and try again.";
    include('error.php');
} else {
    // If valid, update the product in the database

    $query = 'UPDATE comments
              SET 
                  comments = :comments
               WHERE comment_id = :comment_id';
    $statement = $db->prepare($query);
    $statement->bindValue(':comment_id', $comment_id);
    $statement->bindValue(':comments', $comments);
    $statement->execute();
    $statement->closeCursor();

       if ($_SESSION['privilages'] == 1) {
    include ('view_admin.php');
} else {
    include('view_customer.php');
}
}
?>