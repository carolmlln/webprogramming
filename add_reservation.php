<?php
require_once('database.php');
require_once('session.php');
// Get the product data
$customer_id = filter_input(INPUT_POST, 'customer_id',FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$res_id = filter_input(INPUT_POST, 'ticket_id',FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$quantity = filter_input(INPUT_POST, 'quantity', FILTER_VALIDATE_FLOAT,FILTER_SANITIZE_FULL_SPECIAL_CHARS);



// Validate inputs
if ($customer_id == null || $res_id == null || $quantity == false ) {
    $error = "Invalid order data. Check all fields and try again.";
    include('error.php');
} else {
    require_once('database.php');

    // Add the product to the database 
    $query = "INSERT INTO reservations
                 (customer_id,ticket_id,quantity)
              VALUES
                 (:customer_id,:ticket_id,:quantity)";
    $statement = $db->prepare($query);
    $statement->bindValue(':customer_id', $customer_id);
    $statement->bindValue(':ticket_id', $res_id);
    $statement->bindValue(':quantity', $quantity);
    $statement->execute();
    $statement->closeCursor();

    // Display the Product List page
    if ($_SESSION['privilages'] == 1) {
    include ('view_admin.php');
} else {
    include('view_customer.php');
}
}
?>