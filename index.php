<?php
require_once("database.php");
include 'header/header1.php';

?>

            <div class="container">
                <!--Page Heading -->
                <h1 class="mt-4 mb-3">About</h1>
                <div class="row">

                    <!-- Post Content Column -->
                    <div class="col-lg-8">

                        <!-- Preview Image -->
                        <img class="img-fluid rounded" src="images/liverpoolfront.jpg" alt="" width="900" height="600">
                        <!-- Page Content -->
                        <p>Liverpool Football Club is a professional association football club based in Liverpool,
                            Merseyside, England. They compete in the Premier League, the top tier of English football. The club 
                            has won an English record 5 European Cups, 3 UEFA Cups, 3 UEFA Super Cups, 18 League titles, 7 FA 
                            Cups, a record 8 League Cups, and 15 FA Community Shields.</p>

                        <p>The club was founded in 1892 and joined the Football League the following year. The club has played
                            at Anfield since its formation.Liverpool established itself as a major force in both English and 
                            European football during the 1970s and 1980s when Bill Shankly and Bob Paisley led the club to 11 
                            League titles and seven European trophies. Under the management of Rafa Benítez and captained by 
                            Steven Gerrard Liverpool became European champion for the fifth time, winning the 2005 UEFA Champions
                            League Final against Milan in spite of being 3–0 down at half time.</p>

                        <p> Liverpool was the ninth highest-earning football club in the world for 2015–16, with an annual 
                            revenue of €403.8 million,[3] and the world's eighth most valuable football club in 2017, valued
                            at $1.492 billion.[4] The club holds many long-standing rivalries, most notably the North West 
                            Derby against Manchester United and the Merseyside derby with Everton.</p>

                        <p> The club's supporters have been involved in two major tragedies. The first was the Heysel Stadium
                            disaster in 1985, where escaping fans were pressed against a collapsing wall at the henceforth 
                            renamed King Baudouin Stadium in Brussels, with 39 people—mostly Italians and Juventus fans—dying,
                            after which English clubs were given a five-year ban from European competition. The second was the 
                            Hillsborough disaster in 1989, where 96 Liverpool supporters died in a crush against perimeter fencing.</p>

                        <p> The team changed from red shirts and white shorts to an all-red home strip in 1964 which has been
                            used ever since. The club's anthem is "You'll Never Walk Alone".</p>
                    </div>
                    <!-- Sidebar Widgets Column -->
                    <div class="col-md-4">


                        <!-- Categories Widget -->
                        <div class="card my-4">
                            <h5 class="card-header">Useful Sites</h5>
                            <div class="card-block">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <ul class="list-unstyled mb-0">
                                            <li><a target="_blank"href="http://www.liverpoolfc.com/welcome-to-liverpool-fc">Liverpool FC </a></li>
                                            <li><a target="_blank"href="http://www.liverpoolfc.com/history/timeline">Liverpool FC History</a></li>
                                            <li><a target="_blank"href="https://store.liverpoolfc.com/">Liverpool Shop</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-6">

                                        <ul class="list-unstyled mb-0">
                                            <li><a target="_blank" href="https://www.livefootballtickets.com/english-premiership/liverpool-tickets.html?gclid=CjwKCAiA9MTQBRAREiwAzmytw_UmZUlzm2cn9gUnGEfcV0GO9Ck-EG7piVDXE95n-mv6Oq8_4V2cZhoCwwoQAvD_BwE">Live Football Tickets</a></li>
                                            <li><a target="_blank"href="https://www.ticketgum.com/liverpool-tickets?cmpn=Liverpool_world_exact&gclid=CjwKCAiA9MTQBRAREiwAzmytw98y8LdEEp5h4OxsHsthqslywMuTtReDHtFzWMHdku3dIBdK5El52RoCJ_cQAvD_BwE">Ticket Gum</a></li>
                                            <li><a target="_blank"href="http://www.liverpoolfc.com/tickets/tickets-availability">Liverpool FC Shop</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>


                   

                        <!-- Side Widget -->
                        <div class="card my-4">
                            <h5 class="card-header">login</h5>
                            <div class="signin-form">

                                <div class="container small-container">


                                    <form class="log" action="login.php" method="POST" id="login">
                                        <div class="modal-body">
                                            <div id="div-login-msg">
                                                <div id="icon-login-msg" class="glyphicon glyphicon-chevron-right"></div>
                                                <span id="text-login-msg">Type your username and password.</span>
                                            </div>
                                            <input id="login_username" name="email" class="form-control" type="email" required pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,3}$" placeholder="email" title="please enter valid email" required>
                                            <input id="login_password" name="password" class="form-control" type="password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"  placeholder="Password" title="please enter password starting with a capital with at least one capital at least one small case &#13; and at least one number with a length of at least 8" required>
                                        </div>
                                        <div class="modal-footer">
                                            <div>
                                                <button name="" type="submit" class="btn btn-primary btn-group-justified btn-success">Login</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>



                        </div><!-- End row -->

                    </div> 
                    <button onclick="topFunction()" id="btn" title="Go to top" >Go to top</button>
                </div>
                
<?php include('footer/footer.php');?>
                



