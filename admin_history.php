<?php
require_once('database.php');
require_once('session.php');
//include 'header/header3.php';
if (isset($_SESSION['privilages'])) {
    if ($_SESSION['privilages'] == 0) {
        include_once 'user_history.php';
    } else if ($_SESSION['privilages'] == 1) {
      include 'header/header3.php'; 
    ?>


            <div class="container">
                <h3> Logged in as <?php echo $_SESSION['name']?></h3>
                <!--Page Heading -->
                <h1 class="mt-4 mb-3">History</h1>
                <div class="row">

                    <!-- Post Content Column -->
                    <div class="col-lg-8">


                        <img class="img-fluid rounded" src="images/history.jpg" alt="" width="900" height="600">

                        <p>Following the moving of Everton to Goodison Park in 1892, the club's then-president John 
                            Houlding suddenly found himself with the rights to Anfield stadium and no team to play on it.
                            Always a practical man, Houlding quickly decided to circumvent this problem by forming his 
                            own club – Liverpool. </p>

                        <p>After becoming a member of the Football League in 1893, the team managed to get promoted to
                            first division after one season in the second division. Early on, Liverpool established 
                            themselves as one of England's top clubs, winning League titles in 1901, 1906, 1922 and 1923.</p>

                        <h2>The Bill Shankly era</h2>
                        <p>Though an immensely popular club even then, Liverpool FC were not particularly consistent in 
                            the post-WWII period. After claiming their fourth League title in 1947, the club entered a 
                            period of mediocrity which culminated with their relegation to Second Division in 1954. 
                            Things took a turn for the better after Bill Shankly was hired as manager, however; Shankly's
                            first order of business was to release the entire first team squad. He then turned the club's 
                            storage room into the famous "Boot Room", a place that would serve as the coaches' secret 
                            meeting place for the next three decades.</p>

                        <p> Shankly's unorthodox methods bore fruit soon enough. After making their way back to the First 
                            Division in 1962, Liverpool won the League two years later. During the reminder of Shankly's 
                            tenure as manager, they claimed an additional two League titles (1966, 1973), two FA Cups 
                            (1965, 1974), as well as their first European trophy – the 1973 UEFA Cup. In 1974, Shankly
                            resigned from his job due to a need for a break, leaving the club in the hands of his assistant,
                            Bob Paisley.</p>

                        <h2>Adding more trophies to the collection</h2>
                        <p>The change of personnel did not do much to stop Liverpool's dominance. Under Paisley, Liverpool
                            were a remarkably consistent team; during his nine years in charge, the club won an incredible 
                            six League titles and three League Cups. Their reign spread over Europe as well, with Liverpool
                            winning one UEFA Cup and three European Cups in the period between 1976 and 1981. After Paisley's
                            retirement in 1983, his assistant Joe Fagan continued the successful tradition by leading the 
                            team to a Treble in his first season in charge.</p>


                        <h2>The Heysel and Hillsborough tragedies</h2>
                        <p>In 1985, Liverpool faced Juventus in the European Cup final. Unfortunately, that match is now 
                            widely known as the scene of the Heysel disaster; after some unruly fans caused a perimeter wall
                            to collapse, 39 fans were crushed to death. In the aftermath, considering the blame for the 
                            accident was placed solely on Liverpool fans, all English clubs were banned from European 
                            competitions for the next five years.</p>

                        <p>In the absence of European games, Liverpool started focusing on the domestic competitions. 
                            But after winning two more League titles in 1986 and 1988 and an FA Cup in 1985, tragedy struck 
                            again. In the 1989 FA Cup semi-final between Liverpool and Nottingham Forest, 94 fans died in 
                            a crowd crush only six minutes after the game had started. To this day, the Hillsborough disaster
                            remains the worst stadium disaster in English football.</p> 

                        <p>After claiming their ninth League title in 1990, Liverpool entered a downward spiral. With only
                            a couple of Cup trophies and some mediocre league finishes in the 90s and early 00s, it seemed 
                            like Liverpool's star had waned. Nevertheless, they proved their mettle in the most exhilarating
                            way possible; after reaching the Champions League final in 2005, they recovered from 3-0 down 
                            at half-time to eventually beat Milan on penalties. </p>

                        <p>As it turned out, this extraordinary victory was the club's swan song. During the next decade, 
                            Liverpool were mostly seen playing second fiddle to other English clubs, with only two Cup 
                            trophies and a second-place league finish to show for their efforts.</p>
                    </div>
                    <!-- Sidebar Widgets Column -->
                    <div class="col-md-4">


                        <!-- Categories Widget -->
                        <div class="card my-4">
                            <h5 class="card-header">Useful Sites</h5>
                            <div class="card-block">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <ul class="list-unstyled mb-0">
                                            <li><a target="_blank"href="http://www.liverpoolfc.com/welcome-to-liverpool-fc">Liverpool FC </a></li>
                                            <li><a target="_blank"href="http://www.liverpoolfc.com/history/timeline">Liverpool FC History</a></li>
                                            <li><a target="_blank"href="https://store.liverpoolfc.com/">Liverpool Shop</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-6">

                                        <ul class="list-unstyled mb-0">
                                            <li><a target="_blank" href="https://www.livefootballtickets.com/english-premiership/liverpool-tickets.html?gclid=CjwKCAiA9MTQBRAREiwAzmytw_UmZUlzm2cn9gUnGEfcV0GO9Ck-EG7piVDXE95n-mv6Oq8_4V2cZhoCwwoQAvD_BwE">Live Football Tickets</a></li>
                                            <li><a target="_blank"href="https://www.ticketgum.com/liverpool-tickets?cmpn=Liverpool_world_exact&gclid=CjwKCAiA9MTQBRAREiwAzmytw98y8LdEEp5h4OxsHsthqslywMuTtReDHtFzWMHdku3dIBdK5El52RoCJ_cQAvD_BwE">Ticket Gum</a></li>
                                            <li><a target="_blank"href="http://www.liverpoolfc.com/tickets/tickets-availability">Liverpool fC Shop</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>





                </div><!-- End row -->
<button onclick="topFunction()" id="btn" title="Go to top" >Go to top</button>
            </div>           
<?php include('footer/footer.php');
}
} else {
    
    include_once 'index.php';
}
?>