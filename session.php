<?php
//session_start();
//if( !isset( $_SESSION) || time() - $_SESSION['time_out'] > 60)
//{
//    require_once  'time_out.php';
//    echo "<SCRIPT LANGUAGE='JavaScript'> alert('looks like youve timed out please log back in');  </SCRIPT>";
//}
//else
//{
//    
//    $_SESSION['time_out'] = time();
//    
//}
session_start();
 
//Expire the session if user is inactive for 30
//minutes or more.
$expireAfter = 10000;
 
//Check to see if our "last action" session
//variable has been set.
if(isset($_SESSION['time_out'])){
    
    //Figure out how many seconds have passed
    //since the user was last active.
    $secondsInactive = time() - $_SESSION['time_out'];
    
    //Convert our minutes into seconds.
    $expireAfterSeconds = $expireAfter * 60;
    
    //Check to see if they have been inactive for too long.
    if($secondsInactive >= $expireAfterSeconds){
        //User has been inactive for too long.
        //Kill their session.
        
        echo "<SCRIPT LANGUAGE='JavaScript'> alert('looks like youve timed out please log back in');  </SCRIPT>";
        session_unset();
        session_destroy();
        include 'index.php';
        exit();
        
    }
    
    
}
 
//Assign the current timestamp as the user's
//latest activity
$_SESSION['time_out'] = time();

?>

