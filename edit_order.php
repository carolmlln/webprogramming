<?php
require_once('database.php');
require_once('session.php');

$res_id = filter_input(INPUT_POST, 'res_id', FILTER_VALIDATE_INT,FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$customer_id = filter_input(INPUT_POST, 'customer_id', FILTER_VALIDATE_INT,FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$ticket_id = filter_input(INPUT_POST, 'ticket_id', FILTER_VALIDATE_INT,FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$quantity = filter_input(INPUT_POST, 'quantity', FILTER_VALIDATE_INT,FILTER_SANITIZE_FULL_SPECIAL_CHARS);

// Validate inputs
if (empty($customer_id)|| empty($ticket_id) || empty($quantity) || $quantity <= 0) {
     echo "<SCRIPT LANGUAGE='JavaScript'> alert('Invalid product data. Check all fields and try again.!');</SCRIPT>";
    include('edit_order_form.php');
}
   
else {
    // If valid, update the product in the database
    
    $query = 'UPDATE reservations
              SET 
                  quantity = :quantity
               WHERE res_id = :res_id';
    $statement = $db->prepare($query);
    $statement->bindValue(':res_id', $res_id);
    $statement->bindValue(':quantity', $quantity);
    $statement->execute();
    $statement->closeCursor();

       if ($_SESSION['privilages'] == 1) {
    include ('view_admin.php');
} else {
    include('view_customer.php');
}
}
?>